#!/usr/bin/sh

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar main &

if [[ $(xrandr -q | grep -e 'HDMI-1-0 connected' -e 'DP-1-1 connected') ]]; then
	polybar external &
fi
