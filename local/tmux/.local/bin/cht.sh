#!/usr/bin/bash

langs=$(echo "go rust bash" | tr " " "\n")
core_utils=$(echo "xargs sed awk" | tr " " "\n")
selected=$(echo -e "$langs\n$core_utils" | fzf)

read -p "Query: " query

if echo "$langs" | grep -qs $selected; then
	query=`echo $query | tr " " "+"`
 	tmux neww bash -c "echo \"curl cht.sh/$selected/$query/\" & curl cht.sh/$selected/$query & while [ : ]; do sleep 1; done"
else
	tmux neww -c "curl -s cht.sh/$selected~$query | less"
fi
